![Banner](http://www.madarme.co/portada-web.png)
# Titulo del proyecto
#### Curriculum vitae

## Indice 
1. [Caracteristicas del proyecto](#caracteristicas)
2. [Contenido del proyecto](#contenido-del-proyecto)
3. [Tecnologias](#tecnologias)
4. [IDE](#ide)
5. [Instalacion](#instalacion)
6. [Demo](#demo)
7. [Autor](#autor)
8. [Institucion](#institucion)




#### Caracteristicas
- Uso de CSS recomendado: [ver](https://gitlab.com/GenesisDaniela/curriculum_vitae/-/tree/master/css/1)
#### Contenido del proyecto
- [index.html](https://gitlab.com/GenesisDaniela/curriculum_vitae/-/blob/master/index.html): Este archivo visualiza la pagina principal.
#### Tecnologias
- Uso de Bootstrap: [ver](https://getbootstrap.com/)
- Usted puede aprender Bootstrap [Aquí](https://www.youtube.com/watch?v=ZuOL_DoaG9k)
#### IDE
- sublime Text [Ir](https://www.youtube.com/watch?v=rA7VucHRrRw)
#### Instalacion
1. local
    - Descarga del repositorio [Descargar](https://gitlab.com/GenesisDaniela/curriculum_vitae/-/archive/master/curriculum_vitae-master.zip)
    - Abrir el archivo index.html desde el navegador predeterminado
2. Gitlab 
    - Realizar un Fork.
#### Demo
Puede visualizar la version Demo [Aquí](https://genesisdaniela.gitlab.io/curriculum_vitae)
#### Autor
Realizado por: [Genesis Vargas](<genesisdanielavjau@ufps.edu.co>)
#### Institucion
Proyecto desarrollado en la materia de programacion web - [Ingenieria de sistemas](https://ww2.ufps.edu.co/oferta-academica/ingenieria-de-sistemas) de la [Universidad Francisco de Puala Santander](https://ww2.ufps.edu.co/)
